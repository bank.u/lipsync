using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;


[Serializable]
class PhoneAtTime
{
    public float time;
    public string phone;
}

static class JsonArrayHelper
{
    public static T[] ArrayFromJsonString<T>(string jsonArrayString)
    {
        Debug.Log("jsonArrayString length: "+jsonArrayString.Length);
        var jsonString = "{\"Items\":" + jsonArrayString + "}";
        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(jsonString);
        Debug.Log("JsonUtility.FromJson result: "+wrapper);
        var result = wrapper.Items;
        Debug.Log("Result item Length: "+result.Length);
        return result;
    }

    [Serializable]
    private class Wrapper<T>
    {
        public T[] Items;
    }
}

public class MouthSync : MonoBehaviour
{
    public float timeOffset;
    private AudioSource _voiceAudio;
    public TextAsset transcriptJson;
    
    public Sprite phoneSh;
    public Sprite phoneREST;
    public Sprite phoneU;
    public Sprite phoneAR;
    public Sprite phoneO;
    public Sprite phoneI;
    public Sprite phoneS;
    public Sprite phoneN;
    public Sprite phoneH;
    public Sprite phoneM;
    public Sprite phoneB;
    public Sprite phoneF;
    public Sprite phoneOu;
    public Sprite phoneR;
    public Sprite phoneA;
    public Sprite phoneTh;
    public Sprite phoneEh;
    public Sprite phoneK;
    public Sprite phoneW;
    public Sprite phoneL;
    public Sprite phoneV;
    public Sprite phoneD;
    public Sprite phoneT;
    public Sprite phoneEe;
    public Sprite phoneZ;
    public Sprite phoneE;
    public Sprite phoneP;
    public Sprite phoneY;
    public Sprite phoneOh;
    public Sprite phoneG;
    public Sprite phoneUh;
    public Sprite phoneOo;
    public Sprite phoneUnknown;

    private Dictionary<string, Sprite> _phonesToSprites;
    
    private float _playStartTime = 0;
    private List<PhoneAtTime> _transcriptList;
    private int _currentIndex = 0;
    private PhoneAtTime _currentItem;

    // Start is called before the first frame update
    void Start()
    {
        _phonesToSprites = new Dictionary<string, Sprite>()
        {
            { "REST", phoneREST },
            { "SH", phoneSh },
            { "U", phoneU },
            { "AR", phoneAR },
            { "O", phoneO },
            { "I", phoneI },
            { "S", phoneS },
            { "N", phoneN },
            { "H", phoneH },
            { "M", phoneM },
            { "B", phoneB },
            { "F", phoneF },
            { "OU", phoneOu },
            { "R", phoneR },
            { "A", phoneA },
            { "TH", phoneTh },
            { "EH", phoneEh },
            { "K", phoneK },
            { "W", phoneW },
            { "L", phoneL },
            { "V", phoneV },
            { "D", phoneD },
            { "T", phoneT },
            { "EE", phoneEe },
            { "Z", phoneZ },
            { "E", phoneE },
            { "P", phoneP },
            { "Y", phoneY },
            { "OH", phoneOh },
            { "G", phoneG },
            { "UH", phoneUh },
            { "OO", phoneOo },
            { "UNKNOWN", phoneUnknown },
        };

        PhoneAtTime[] transcriptArr = JsonArrayHelper.ArrayFromJsonString<PhoneAtTime>(transcriptJson.text);
        Debug.Log("transcriptArr Length: "+transcriptArr.Length);
        _transcriptList = new List<PhoneAtTime>(transcriptArr);
        _transcriptList = _transcriptList.OrderBy(o => o.time).ToList();
        Debug.Log("_transcriptList Length: "+_transcriptList.Count);
        Debug.Log("_transcriptList Phone 0: "+_transcriptList[0].phone);
        Debug.Log("_transcriptList Time 0: "+_transcriptList[0].time);
        
    }

    private void OnEnable()
    {
        _playStartTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if ((_transcriptList == null) || (_currentIndex == _transcriptList.Count - 1))
        {
            return;
        }
        var now = Time.time - _playStartTime;
        var nextItem = _transcriptList[_currentIndex + 1];
        while (nextItem.time < now)
        {
            //step forwards
            _currentIndex++;
            if (_currentIndex == _transcriptList.Count - 1)
            {
                // Fin
                return;
            }
            nextItem = _transcriptList[_currentIndex + 1];
        }
        Debug.Log("Current index now "+_currentIndex);
        var currentItem = _transcriptList[_currentIndex];
        if (currentItem == _currentItem)
        {
            return;
        }
        _currentItem = currentItem;
        UpdateToCurrentItem();
    }

    private void UpdateToCurrentItem()
    {
        Debug.Log("Updating to "+_currentIndex);
        var sprite = _phonesToSprites[_currentItem.phone];
        GetComponent<SpriteRenderer>().sprite = sprite;
    }
}